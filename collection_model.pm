#!/usr/bin/perl

package collection_model;

use LWP::UserAgent;
use JSON::PP qw(decode_json);

our %DATA_TYPE;

sub get_sshost {
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/collection/getSSHost");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        return 0, 0;
    }

    my $resOut = $res->content;
    $resOut =~ s/"//g;

    if($resOut eq "null" || !$res->content){
        return 0, 0;
    }

    my @output = split (/\|/, $resOut);

    return scalar @output, \@output;
}

sub get_ssuser {
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/collection/getSSUser");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        return 0, 0;
    }

    my $resOut = $res->content;
    $resOut =~ s/"//g;

    if($resOut eq "null" || !$res->content){
        return 0, 0;
    }

    my @output = split (/\|/, $resOut);

    return scalar @output, \@output;
}

sub get_sspass {
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/collection/getSSPass");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        return 0, 0;
    }

    my $resOut = $res->content;
    $resOut =~ s/"//g;

    if($resOut eq "null" || !$res->content){
        return 0, 0;
    }

    my @output = split (/\|/, $resOut);

    return scalar @output, \@output;
}

sub get_sshome {
	my @_sshome = ();

    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");
    
    # Create a request
    my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/collection/getSSHome");
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        return 0, 0;
    }

    my $resOut = $res->content;
    $resOut =~ s/"//g;

    if($resOut eq "null" || !$res->content){
        return 0, 0;
    }

    foreach my $f_id(split(/\|/,$resOut)){
        # Create a request
        my $req = HTTP::Request->new(GET => $common::config->{dbapiurl}."process/collection/getPathByFiletype/".$resOut);
        $req->content_type('application/x-www-form-urlencoded');
        
        # Pass request to the user agent and get a response back
        my $res = $ua->request($req);
        
        # Check the outcome of the response
        if (!$res->is_success) {
            return 0, 0;
        }

        my $pathfile = $res->content;
        $pathfile =~ s/"//g;

        if($pathfile eq "null" || !$res->content){
            next;
        }

	    push @_sshome, $resOut.";".$pathfile;
    }

    return scalar @_sshome, \@_sshome;
}

sub get_sstarget {
	my ($filetype, $datatype) = @_;
    
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new( GET => $common::config->{dbapiurl}."process/collection/getSSTarget/".$filetype."/".$datatype );
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        return 0;
    }

    if($res->content eq "null"){
        return 0;
    }
    
    my $jsonResp = decode_json $res->content;
    
    $common::config->{"sstarget"}{$jsonResp->{'filetype'}}{$jsonResp->{'datatype_id'}} = $jsonResp->{'pathfile'};
    $common::config->{"sstarget"}{"ft_".$jsonResp->{'filetype'}}{$jsonResp->{'datatype_id'}} = $jsonResp->{'filetype_id'};

	if (!-e $jsonResp->{'pathfile'}){
		print "[ Creating Directory ] : $jsonResp->{'pathfile'}....\n";
		common::mkdir_r($jsonResp->{'pathfile'}, 0777);
	}

    return $jsonResp->{'pathfile'};
}

sub get_datatype {
    print "[ Getting Data Type From Database ] : ";	
	%DATA_TYPE = ();
    
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new( GET => $common::config->{dbapiurl}."configuration/getDataType" );
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request data type to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "Failed, no data type found in database\n";
        return 0;
    }
    
    my $jsonResp = decode_json $res->content;

    foreach my $output(@$jsonResp){
		$DATA_TYPE{$output->{data_type}} = $output->{data_type_id};
    }

    print "Success\n";

    return 1;
}

sub insert_process {
	print "[ Inserting Process To Database ] : ";
	
	my ($name_file_ctrl, $periode, $type_file_ctrl, $filename_in_ctrl, $record_in_ctrl, $target_dir, $filetype, $data_type_id, $day, $flag_imsi, $process_status_id, $err_msg, $post_pre) = @_;

    my $closeJSON = '{
            "periode":'.$periode.',
            "data_type_id":'.$data_type_id.',
            "target_dir":"'.$target_dir.'",
            "filetype":'.$filetype.',
            "filename_in_ctrl":"'.$filename_in_ctrl.'",
            "day":"'.$day.'",
            "flag_imsi":'.$flag_imsi.',
            "process_status_id":'.$process_status_id.',
            "error_message":"'.$err_msg.'",
            "post_pre":"'.$post_pre.'"
        }';
    
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new(POST => $common::config->{dbapiurl}."process/collection/closeProcess");
    $req->content_type('application/json');
    $req->content($closeJSON);
    
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, cannot insert process to database\n";
        return 0;
    }

    print "Success\n";

    return 1;
}

sub get_alert_jobs {
    print "[ Get Jobs Alert ] : ";    
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new( GET => $common::config->{dbapiurl}."process/alert/getProcess" );
    $req->content_type('application/x-www-form-urlencoded');
    
    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, unable to request alert jobs to database\n";
        return 0;
    }

    if($res->content eq "null"){
        print "No alert jobs\n";
        return 0;
    }
    
    my $jsonResp = decode_json $res->content;

    print "Success\n";

    return $jsonResp;
}

    
sub updateLastAlert {
    my($lastAlert) = @_;
    print "[ Updating Last Alert Date in Database To : ".$lastAlert." ] : ";
    
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new(PUT => $common::config->{dbapiurl}."process/alert/updateLastAlert/".$lastAlert);
    $req->content_type('application/json');
    $req->content($closeJSON);
    
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, cannot Update Last Alert\n";
        return 0;
    }

    print "Success\n";

    return 1;
}

sub makeEmailTask {
    print "  > [ Make Email Task ] : ";
	my ($job) = @_;
    
    my $emailTask = '{
        "mailconf_id" :'.$job->{mailconf_id}.',
        "mailtask_title":"'.$job->{mailconf_title}.'",
        "mailtask_subject":"'.$job->{mailconf_subject}.'",
        "mailtask_message":"'.$job->{mailconf_message}.'",
        "mailtask_attachment":"",
        "mailtask_send_date":"'.common::getDate('%Y-%m-%d %H:%M:%S').'",
        "process_id":""
        }';
    
    my $ua = LWP::UserAgent->new;
    $ua->agent("MyApp/0.1 ");

    my $req = HTTP::Request->new(POST => $common::config->{dbapiurl}."process/alert/makeTask");
    $req->content_type('application/json');
    $req->content($emailTask);
    
    my $res = $ua->request($req);
    
    # Check the outcome of the response
    if (!$res->is_success) {
        print "Failed, cannot make mail task\n";
        return 0;
    }

    print "Success\n";

    return 1;
}

1;
