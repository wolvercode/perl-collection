#!/usr/bin/perl

package collection;

use collection_model;
use collection_handler_ssh;
use collection_handler_local;

# Collection main function, responsible for managing what to do
sub main {
    if (!collection_model::get_datatype()){
        print "....Process Stopped....\n";
		return 0;
    }
	
	# Do the collection of file
	do_jobs();

    # Do alert if file hasn't send
    alert_file();

	return 1;
}

# Sleeping function
sub sleeping {
	print "....Sleep for ".$common::config->{sleep}." seconds....\n";
	
	sleep($common::config->{sleep});
}

# Function for doing the jobs of collecting file
sub do_jobs {
	# Get target ssh host, username, password, main directory
    print "[ Getting SSH Configuration ] : ";
    my ($count_host, $host) = collection_model::get_sshost();
    if(!$count_host){ 
        print "Failed, no ssh host found in database\n";
        return 0;
    };

    my ($count_user, $user) = collection_model::get_ssuser();
    if(!$count_user){ 
        print "Failed, no ssh user found in database\n";
        return 0;
    };

    my ($count_pass, $pass) = collection_model::get_sspass();
    if(!$count_pass){
        print "Failed, no ssh pass found in database\n";
        return 0;
    };

    my ($count_home, $home) = collection_model::get_sshome();
    if(!$count_home){
        print "Failed, no ssh dir found in database\n";
        return 0;
    };
    print "Success\n";

	# Get/Set directory to keep file Hak & Kewajiban on current server from database
    my $targetocs  = collection_model::get_sstarget("CDR",$collection_model::DATA_TYPE{"OCS"});
    if(!$targetocs){
        print "[ Getting Target Directory ] : Failed, no ocs directory found in database\n";
        return 0;
    }
    my $targettapin = collection_model::get_sstarget("CDR",$collection_model::DATA_TYPE{"TAPIN"});
    if(!$targettapin){
        print "[ Getting Target Directory ] : Failed, no tapin directory found in database\n";
        return 0;
    }
    print "[ Getting Target Directory ] : Success\n";

    if($common::config->{ssmethod} eq "ssh"){
        # Looping list of target ssh host
        for my $i (0 .. @{$#host}) {
            # Connecting to target ssh or skip current loop if unable to connect
            
            my ($filetypeSource, $sourcePath) = (split ";", $home->[$i]);

            my $konek = collection_handler_ssh::conn_ssh($host->[$i], $user->[$i], $pass->[$i], $sourcePath);
            next if !$konek;

            # Get list of control file on target ssh directory or skip current loop if no file found
            my $list_control = collection_handler_ssh::get_list_control();
            next if !$list_control;

            # Get list of file Hak & Kwjb on target ssh directory or skip current loop if no file found
            # my $list_file = collection_handler_ssh::cek_file($list_control);
            # next if !scalar(@{$list_file});

            # Compare actual file record to file record inside control file
            collection_handler_local::compare_file_to_control($sourcePath, $list_control, $filetypeSource);
        }
    }elsif($common::config->{ssmethod} eq "local"){
        my ($filetypeSource, $sourcePath) = (split ";", $home->[0]);
        
        print "[ Collecting From Directory ] : $sourcePath\n";

        # Get list of control file on target ssh directory or skip current loop if no file found
        my $list_control = collection_handler_local::get_list_control($sourcePath);
        return 1 if !$list_control;

        # Get list of file Hak & Kwjb on target ssh directory or skip current loop if no file found
        # my $list_file = collection_handler_local::cek_file($sourcePath, $list_control);
        # return 1 if !scalar(@{$list_file});

        # Compare actual file record to file record inside control file
        collection_handler_local::compare_file_to_control($sourcePath, $list_control, $filetypeSource);
    }else{
        print "Collection method : ".$common::config->{ssmethod}." is unrecognized, change method in collection.conf\n";
    }

    return 1;
}

sub alert_file {
    my $alertJobs = collection_model::get_alert_jobs();
    if(!$alertJobs){
        return 0;
    }

    foreach my $job(@$alertJobs) {
        my $statusCheck = common::check_path($job);
        if (!$statusCheck){
            collection_model::makeEmailTask($job);
        }
    }

    my $alertDate = common::getDate('%Y%m%d%H%M%S');
    collection_model::updateLastAlert($alertDate);
}

1;
