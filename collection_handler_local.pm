#!/usr/bin/perl

package collection_handler_local;

sub get_list_control {
	print "[ Check Control File ] : ";

    my ($uploadFolder) = @_;

	my @listcontrol = ();

	my @ctrls = `ls -1 $uploadFolder | grep '.ctrl\$'`;
    
	if (!@ctrls){
        print "No Control File Found...\n";
        return 0;
	}

    my $i = 0;
    
    foreach my $ctrl(@ctrls){
		chomp($ctrl);
        my $lsnm = `cd $uploadFolder ; cat $ctrl`;
		chomp($lsnm);
        push @listcontrol, $ctrl."|".$lsnm;
        $i++;
    }

    print "Number of control files found => $i\n";

    return \@listcontrol;
}

sub mv_file {
	my ($uploadFolder, $nmfile, $tgfile) = @_;
	
    my $status_move = system("cd $uploadFolder ; mv $nmfile $tgfile");
    if($status_move){
        return 0;
    }

	return 1;
}

sub compare_file_to_control {
	my ($uploadFolder, $list_file, $filetypeSource) = @_;

	print "[ Comparing File to Control File ]\n";
	my @succes_move = ();
	foreach my $l_file(@{$list_file}){
		my @content_ctrl 	 = split(/\|/, $l_file);
		my $name_file_ctrl 	 = $content_ctrl[0];
		my @split_fname_ctrl = split "_", $name_file_ctrl;
		my $type_data 		 = uc($split_fname_ctrl[0]);
		my $send_date 		 = "";
		my $post_pre 		 = "";
		my $flag_imsi		 = 0;
		if($type_data eq "TAPIN"){
			$send_date 		 = $split_fname_ctrl[1];
		}else{
			$send_date 		 = $split_fname_ctrl[2];
			$flag_imsi		 = $split_fname_ctrl[4];
			if($split_fname_ctrl[4]){
				$flag_imsi 		 =~ s/.ctrl//g;
			}else{
				$flag_imsi = "0";
			}
		}
		$send_date =~ s/.ctrl//g;
		my $periode 		 = substr($send_date, 0, 6);
		my $day 	 		 = substr($send_date, -2);
		my $data_type_id  	 = $collection_model::DATA_TYPE{$type_data};
		my $target_dir		 = $common::config->{"sstarget"}{"CDR"}{$data_type_id}.$periode."/";
		if($type_data eq "OCS"){
			# $type_data  = $type_data."_".uc($split_fname_ctrl[1]);
			$post_pre 	= lc($split_fname_ctrl[1]);
			$target_dir = $target_dir.$post_pre."/";
		}
		my $filename_in_ctrl = $content_ctrl[1];
		my $record_in_ctrl	 = $content_ctrl[2];
		$record_in_ctrl =~ s/\n//g;
		my $size_in_ctrl	 = $content_ctrl[3];
		$size_in_ctrl =~ s/\n//g;
		my $filetype 		 = $common::config->{"sstarget"}{"ft_CDR"}{$data_type_id};
		my $failed_ctrl 	 = $name_file_ctrl.".failed";

		my($status_file) = `ls -l $uploadFolder | grep -w $filename_in_ctrl\$ | awk '{print \$9}'`;
		chomp $status_file;

		if(!$status_file){
			print "  > [ File Control : $name_file_ctrl ] => File not found, file rejected\n";
			`cd $uploadFolder ; mv $name_file_ctrl $failed_ctrl`;
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $filename_in_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Actual File Not Found/File Control Only", $post_pre);
			next;
		}
		
		common::mkdir_r($target_dir,0777);

		print "  > [ Record In Control File ] : $record_in_ctrl\n";
	
		my($actual_record) = `cd $uploadFolder ; wc -l < $filename_in_ctrl`;
		chomp $actual_record;
		$actual_record =~ s/\n//g;

		if($record_in_ctrl ne $actual_record){
			print "  > [ Actual Record : $actual_record ] => File record not match, file rejected\n";
			`cd $uploadFolder ; mv $name_file_ctrl $failed_ctrl`;
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $filename_in_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Actual File record count not match with record count in File Control", $post_pre);
			next;
		}

		print "  > [ Actual Record : $actual_record ] => File record match\n";

		print "  > [ Size In Control File ] : $size_in_ctrl\n";
	
		my($actual_size) = `cd $uploadFolder ; wc -c < $filename_in_ctrl`;
		chomp $actual_size;
		$actual_size =~ s/\n//g;

		if($size_in_ctrl ne $actual_size){
			print "  > [ Actual File Size : $actual_size ] => File size not match, file rejected\n";
			`cd $uploadFolder ; mv $name_file_ctrl $failed_ctrl`;
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $filename_in_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Actual File size not match with size in File Control", $post_pre);
			next;
		}

		print "  > [ Actual File Size : $actual_size ] => File size match\n";
		
		print "    > Moving file to dir '$target_dir' => ";
		my $move_status = mv_file($uploadFolder, "$filename_in_ctrl", $target_dir);
		if(!$move_status){
			print "Failed, file rejected\n";
			`cd $uploadFolder ; mv $name_file_ctrl $failed_ctrl`;
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $filename_in_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Failed moving Actual File to Source Directory", $post_pre);
			next;
		}else{
			print "Success\n";
		}

		my $status_insert = collection_model::insert_process($name_file_ctrl, $periode, $type_data, $filename_in_ctrl, 
		$record_in_ctrl, $target_dir, $filetype, $data_type_id, $day, $flag_imsi, 7, "", $post_pre);
		if(!$status_insert){
			`cd $uploadFolder ; mv $name_file_ctrl $failed_ctrl`;
			next;
		}

		`cd $uploadFolder ; rm -f $name_file_ctrl`;
	}
}
1;