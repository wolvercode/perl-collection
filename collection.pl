#!/usr/bin/perl

# Collection made by Fahmy @20190125

use lib './';
use strict;
use warnings;
use common;
use collection;

$SIG{CHLD} = 'DEFAULT';  # turn off auto reaper
$SIG{KILL} = \&signal_handler;
$SIG{INT}  = \&signal_handler;
$SIG{TERM} = \&signal_handler;

# Handler for Ctrl + C keypress
sub signal_handler {
    die "Caught a signal control + C";
}

$| = 1;

print "....Starting Daemon Collection....\n";

while(1) {
    # Loading the configuration file
    if(!common::load_config()){
        print "....Exiting daemon....\n";
        exit;
    }

    # Running the main function
    collection::main();
    
    # Sleep the daemon at the end of loop
    collection::sleeping();
}
