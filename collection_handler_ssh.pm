#!/usr/bin/perl

package collection_handler_ssh;

use Net::OpenSSH;

my $home = "";
my $host = "";

our $ssh;

sub conn_ssh {
    my ($i_host, $i_user, $i_pass, $i_homeDir) = @_;

    print "[ Connecting SSH To ] : { Host : ".$i_host.", User : ".$i_user.", Pass : ".$i_pass.", Directory : ".$i_homeDir." } ";

	$home = "";
	$host = "";

    $ssh = Net::OpenSSH->new($i_host, user => $i_user, password => $i_pass,);
	$ssh->error and die "Couldn't establish SSH connection: ". $ssh->error;

    $home = $i_homeDir;
    $home =~ s/^\s+//;
    $host = $i_host;
    print "=> Success\n";
    
    return 1;
}

sub cmd_ssh($) {
	$a = "'".$home."'";
	$ssh->capture("cd $a ; $_[0]");
}

sub mv_file {
	my ($nmfile, $tgfile) = @_;
	
	if($common::config->{ssmethod} eq "local"){
		my $_file = $home.$nmfile;
		my $status_move = system("mv $_file $tgfile");
		if($status_move){
			return 0;
		}
	}elsif($common::config->{ssmethod} eq "ssh"){
		my ($output) = cmd_ssh("cat $nmfile");
		
		open(my $foutput, ">", $tgfile.$nmfile) or do {
				return 0;
			};
		print $foutput $output;
		close($foutput);

		cmd_ssh("rm -r $nmfile");
	}

	return 1;
}

sub get_list_control {
	print "[ Check Control File ] : ";

	my @listcontrol = ();

	my @ctrls = cmd_ssh("ls -l | grep '.ctrl\$' | awk '{print \$9}'");
    
	if (!@ctrls){
        print "No Control File Found...\n";
        return 0;
	}

    my $i = 0;
    
    foreach my $ctrl(@ctrls){
		chomp($ctrl);
        my ($lsnm) = cmd_ssh("cat $ctrl");

        push @listcontrol, $ctrl."|".$lsnm;
        $i++;
    }

    print "Number of control files found => $i\n";

    return \@listcontrol;
}

sub cek_file {
	print "[ Check File Exist ]\n";

    my $listcontrol = $_[0];

	my @list_file = ();
	foreach my $ctrl(@{$listcontrol}){
		my @content_ctrl 	 = split(/\|/, $ctrl);
		my $name_file_ctrl 	 = $content_ctrl[0];
		my $filename_in_ctrl = $content_ctrl[1];
		my $failed_ctrl 	 = $name_file_ctrl.".failed";

		print "  > [ File Name In Control File ] : $filename_in_ctrl => ";

		my($status_file) = cmd_ssh("ls -l | grep -w $filename_in_ctrl\$ | awk '{print \$9}'");
		chomp $status_file;

		if(!$status_file){
			print "File Not Found\n";
			cmd_ssh("mv $name_file_ctrl $failed_ctrl");
			next;
		}

		print "File Found\n";
		push @list_file, $ctrl;
	}

	return \@list_file;
}

sub compare_file_to_control {
	my ($uploadFolder, $list_file, $filetypeSource) = @_;

	print "[ Comparing File to Control File ]\n";
	my @succes_move = ();
	foreach my $l_file(@{$list_file}){
		my @content_ctrl 	 = split(/\|/, $l_file);
		my $name_file_ctrl 	 = $content_ctrl[0];
		my @split_fname_ctrl = split "_", $name_file_ctrl;
		my $type_data 		 = $split_fname_ctrl[0];
		my $send_date 		 = "";
		my $post_pre 		 = "";
		my $flag_imsi		 = 0;
		if($type_data eq "tapin"){
			$send_date 		 = $split_fname_ctrl[1];
		}else{
			$send_date 		 = $split_fname_ctrl[2];
			$flag_imsi		 = $split_fname_ctrl[4];
			$flag_imsi 		 =~ s/.ctrl//g;
		}
		$send_date =~ s/.ctrl//g;
		my $periode 		 = substr($send_date, 0, 6);
		my $day 	 		 = substr($send_date, -2);
		my $type_file_ctrl 	 = uc($split_fname_ctrl[0]);
		if($type_file_ctrl eq "OCS"){
			$type_file_ctrl  = uc($split_fname_ctrl[0])."_".uc($split_fname_ctrl[1]);
			$post_pre 		 = lc($split_fname_ctrl[1]);
		}
		my $data_type_id  	 = $collection_model::DATA_TYPE{$type_file_ctrl};
		my $filename_in_ctrl = $content_ctrl[1];
		my $record_in_ctrl	 = $content_ctrl[2];
		$record_in_ctrl =~ s/\n//g;
		my $size_in_ctrl	 = $content_ctrl[3];
		$size_in_ctrl =~ s/\n//g;
		my $target_dir		 = $common::config->{"sstarget"}{"CDR"}{$data_type_id}.$periode."/";
		my $filetype 		 = $common::config->{"sstarget"}{"ft_CDR"}{$data_type_id};
		my $failed_ctrl 	 = $name_file_ctrl.".failed";

		my($status_file) = cmd_ssh("ls -l | grep -w $filename_in_ctrl\$ | awk '{print \$9}'");
		chomp $status_file;

		if(!$status_file){
			print "  > [ File Control : $name_file_ctrl ] => File not found, file rejected\n";
			cmd_ssh("mv $name_file_ctrl $failed_ctrl");
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $failed_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Actual File Not Found/File Control Only", $post_pre);
			next;
		}
		
		common::mkdir_r($target_dir,0777);

		print "  > [ Record In Control File ] : $record_in_ctrl\n";
	
		my($actual_record) = cmd_ssh("wc -l < $filename_in_ctrl");
		chomp $actual_record;
		$actual_record =~ s/\n//g;

		if($record_in_ctrl ne $actual_record){
			print "  > [ Actual Record : $actual_record ] => File record not match, file rejected\n";
			cmd_ssh("mv $name_file_ctrl $failed_ctrl");
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $failed_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Actual File record count not match with record count in File Control", $post_pre);
			next;
		}

		print "  > [ Actual Record : $actual_record ] => File record match\n";

		print "  > [ Size In Control File ] : $size_in_ctrl\n";
	
		my($actual_size) = cmd_ssh("wc -c < $filename_in_ctrl");
		chomp $actual_size;
		$actual_size =~ s/\n//g;

		if($size_in_ctrl ne $actual_size){
			print "  > [ Actual File Size : $actual_size ] => File size not match, file rejected\n";
			`cd $uploadFolder ; mv $name_file_ctrl $failed_ctrl`;
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $failed_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Actual File size not match with size in File Control", $post_pre);
			next;
		}

		print "  > [ Actual File Size : $actual_size ] => File size match\n";
		
		print "    > Moving file to dir '$target_dir' => ";
		my $move_status = mv_file("$filename_in_ctrl", $target_dir);
		if(!$move_status){
			print "Failed, file rejected!\n";
			cmd_ssh("mv $name_file_ctrl $failed_ctrl");
			collection_model::insert_process($name_file_ctrl, $periode, $type_data, $failed_ctrl, 
			$record_in_ctrl, $uploadFolder, $filetypeSource, $data_type_id, $day, $flag_imsi, 5, "Failed moving Actual File to Source Directory", $post_pre);
			next;
		}else{
			print "Success\n";
		}

		my $status_insert = collection_model::insert_process($name_file_ctrl, $periode, $type_data, $filename_in_ctrl, 
		$record_in_ctrl, $target_dir, $filetype, $data_type_id, $day, $flag_imsi, 7, "", $post_pre);
		if(!$status_insert){
			cmd_ssh("mv $name_file_ctrl $failed_ctrl");
			next;
		}

		cmd_ssh("rm $name_file_ctrl");
	}
}

1;
